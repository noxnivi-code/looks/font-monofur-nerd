# Font Monofur Nerd

## Description
Monofur typeface was created by Tobias B Koehler and is released as freeware (see licenses inside).

As of now, it's set as the default font for NoxNivi's interface, though the version used is the one provided by Nerd-Fonts (https://github.com/ryanoasis/nerd-fonts). The whole NerdFont package is way too big to be included in an ISO, so it's not provided here. It can be downloaded in the provided link.

The font packaged for NoxNivi does not include the Windows compatible version. You can get that directly from the Nerd-Fonts repository at github (the link provided before).

<!--
## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
-->

## Screenshots

## Installation
- Copy the font to `~/fonts/` and select it within your software or system preferences to apply system wide.
- If you are using Arch Linux, add the NoxNivi repository and install with `pacman -S font-monofur-nerd`. Then it will be available to all users.

## Usage

<!--
## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.


## Roadmap


## Contributing

## Authors and acknowledgment


## License

## Project status
-->
